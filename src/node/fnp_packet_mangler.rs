/* Fred-rs freenet clone
 *   Copyright (C) 2022  Michael Mestnik <cheako+fred@mikemestnik.net>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#![allow(unreachable_code)]

use aes::cipher::{KeyIvInit};
type Aes128Ctr64LE = ctr::Ctr64LE<aes::Aes128>;

use std::sync::Once;

use super::peer::Peer;

use ring::digest;
use ring::digest::digest;
use ring::rand::{self, Random, SecureRandom};
use tokio::net::{ToSocketAddrs, UdpSocket};

struct AnonPacket<'a, A, B>
where
    A: ToSocketAddrs,
    B: ToSocketAddrs,
{
    version: i32,
    neg_type: i32,
    phase: i32,
    setup_type: i32,
    data: &'a [u8],
    pn: Option<Peer<A>>,
    reply_to: Peer<B>,
    cipher: digest::Algorithm,
}

impl<'a, A, B> AnonPacket<'a, A, B>
where
    A: ToSocketAddrs,
    B: ToSocketAddrs,
{
    async fn send(
        self,
        sock: &UdpSocket,
        rng: &impl SecureRandom,
        fast_rng: &impl ::rand::Rng,
    ) -> Result<(), ring::error::Unspecified> {
        send_packet(
            sock,
            rng,
            fast_rng,
            &[
                &[
                    self.version as u8,
                    self.neg_type as u8,
                    self.phase as u8,
                    self.setup_type as u8,
                ],
                self.data,
            ]
            .concat(),
            self.cipher,
            self.pn,
            self.reply_to,
            true,
        )
        .await
    }
}

async fn send_packet<A, B>(
    _sock: &UdpSocket,
    rng: &impl SecureRandom,
    _fast_rng: &impl ::rand::Rng,
    output: &[u8],
    _cipher: digest::Algorithm,
    _pn: Option<Peer<A>>,
    _reply_to: Peer<B>,
    _anon_auth: bool,
) -> Result<(), ring::error::Unspecified>
where
    A: ToSocketAddrs,
    B: ToSocketAddrs,
{
    static START: Once = Once::new();

    START.call_once(|| {
        log::trace!("Skip checking length.");
    });

    let _iv: Random<[u8; 16]> = rand::generate(rng)?;
    let _hash = digest(&digest::SHA256, output);
    let pre_padding_length = 34 + output.len();
    const MAX_PACKET_SIZE: usize = 65_507;
    let _padding_length: usize =
        num::traits::clamp_max(MAX_PACKET_SIZE.saturating_sub(pre_padding_length), 100);

    // let mut cipher = Aes128Ctr64LE::new();

    Ok(())
}

async fn send_jfk_message_one_auth<A>(
    _sock: &UdpSocket,
    _peer: &Peer<A>,
    _setup_type: i32,
    _neg_type: i32,
) where
    A: ToSocketAddrs,
{
    [32, 91, 32];
    todo!()
}

async fn send_jfk_message_one<A>(
    sock: &UdpSocket,
    peer: &Peer<A>,
    unknown_initiator: bool,
    setup_type: i32,
    neg_type: i32,
) where
    A: ToSocketAddrs,
{
    if unknown_initiator {
        return send_jfk_message_one_auth(sock, peer, setup_type, neg_type).await;
    };

    [32, 91];
    todo!()
}

pub async fn send_handshake<A>(sock: &UdpSocket, peer: &Peer<A>)
where
    A: ToSocketAddrs,
{
    send_jfk_message_one(
        sock,
        peer,
        peer.handshake_unknown_initiator(),
        peer.handshake_setup_type(),
        peer.get_neg_type(),
    )
    .await
}

pub fn add(left: usize, right: usize) -> usize {
    left + right
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = add(2, 2);
        assert_eq!(result, 4);
    }
}
