/* Fred-rs freenet clone
 *   Copyright (C) 2022  Michael Mestnik <cheako+fred@mikemestnik.net>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::collections::VecDeque;

use tokio::net::ToSocketAddrs;

type Nonce = [u8; 16];

#[allow(dead_code)]
pub struct Peer<A: ToSocketAddrs> {
    // pub neg_types: Box<[i32]>, only valid value 10
    addr: A,
    is_client: bool,
    jfk_nonces_sent: VecDeque<Nonce>,
}

mod impls {
    use std::collections::VecDeque;

    use super::Peer;
    use tokio::net::ToSocketAddrs;

    impl<A> Peer<A>
    where
        A: ToSocketAddrs,
    {
        pub fn new(addr: A, is_client: bool) -> Self {
            Self {
                addr,
                is_client,
                jfk_nonces_sent: VecDeque::with_capacity(10),
            }
        }

        pub fn get_neg_type(&self) -> i32 {
            10
        }

        pub fn get_neg_types(&self) -> &'static [i32] {
            &[10]
        }

        pub fn should_send_handshake(&self) -> bool {
            true
        }

        pub fn get_handshake_addr(&self) -> &A {
            &self.addr
        }

        pub fn handshake_unknown_initiator(&self) -> bool {
            self.is_client
        }

        pub fn handshake_setup_type(&self) -> i32 {
            if self.is_client {
                1
            } else {
                -1
            }
        }
    }
}
